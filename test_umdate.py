import unittest
from umdate import Person, matches, display_matches, read_file, add_user
from pathlib import Path

class TestUmdate(unittest.TestCase):
    '''Tests the Person class methods and the matches, 
    display_matches, read_file, and add_user functions'''
    
    def setUp(self):
        '''
        Attributes:
            ethan (instance of class): male, interested in females, won't date 
                                        someone who smokes
            amber (instance of class): female, interested in males, won't date
                                        someone who smokes
            numerah (instance of class): female, interested in both males and 
                                        females, won't date someone who smokes
                                        or drinks and lives off campus
            sharon (instance of class): female, interested in males, won't date
                                        someone who lives off campus
            josh (instance of a class): male, interested in females, won't date
                                        someone who drinks or has a pet
            brick (instance of a class): male, interested in males, does not
                                        have any dealbreakers 
        '''
        self.ethan = Person('Ethan', 21, ['a','a','c','a','a','a','a','a','c',
                                          'b','b','b','a','a','b','a','a','b',
                                          'a','b'], ['b','a','a','a'],
                            'ethan@umd.edu',"I am Ethan")
        self.amber = Person('Amber', 20, ['b','b','b','a','b','c','a','c','b',
                                          'a','b','a','a','c','a','c','b','a',
                                          'a','a'],['b','a','a','a'],
                            'amber@umd.edu','I am Amber')
        self.numerah = Person('Numerah', 22, ['b','c','c','a','a','a','a','a',
                                              'c','b','b','b','a','a','b','a',
                                              'b','b','a','b'], 
                              ['a','a','a','a'],
                              'numerah@umd.edu','I am Numerah')
        self.sharon = Person('Sharon', 20, ['b','b','b','b','b','b','b','b','b',
                                            'b','b','b','b','b','b','b','b','b',
                                            'b','b'], ['a','a','a','b'],
                            'sharon@umd.edu', 'I am Sharon')
        self.josh = Person('Josh', 20, ['a','a','b','b','a','a','a','a','c',
                                          'b','b','b','a','a','b','a','a','b',
                                          'a','b'], ['a','b','b','a'],
                            'josh@umd.edu','I am Josh')
        self.brick = Person('Brick', 19, ['a','b','b','b','b','b','b','b','b',
                                          'b','b','b','b','b','b','b','b','b',
                                          'b','b'],['a','a','a','a'],
                            'brick@umd.edu','I am brick')      
        
    def test_name(self):
        '''Tests that the name attribute is correct'''
        # name should be the first argument when the class is instatiated
        self.assertEqual(self.ethan.name, 'Ethan')
        
        # name should not have 'Name:' in front of it
        self.assertNotEqual(self.ethan.name, 'Name:Ethan') 
        
        # name should not be a number 
        self.assertNotEqual(self.amber.name, 20)
        
    def test_questionnare(self):
        '''Tests that the answers attribute is correct'''
        # ethan's first answer should be 'a'
        self.assertEqual('a', self.ethan.answers[0])
        
        # ethan should not have a number in his answers
        self.assertNotIn(1, self.ethan.answers)
        
        # ethan should not have anything other than a,b,c,d,e, or f in answers
        self.assertNotIn('f', self.ethan.answers[3])
        
    def test_gender(self):
        '''Tests that the user's gender attribute is correct'''
        # gender should be 'Male'
        self.assertEqual(self.ethan.gender, 'Male')
        
        # gender should not be 'M'
        self.assertNotEqual(self.ethan.gender, 'M')
        
        # gender should be 'Female'
        self.assertEqual(self.amber.gender, 'Female')
        
        # gdner should not be 'F'
        self.assertNotEqual(self.amber.gender, 'F')
        
        self.assertEqual(self.josh.gender, 'Male')
        self.assertEqual(self.brick.gender, 'Male')
        
    def test_email(self):
        '''Tests that the user's email is correct'''
        self.assertEqual(self.ethan.email, 'ethan@umd.edu')
        
        # user's email should not contain 'Email:' 
        self.assertNotEqual(self.ethan.email, 'Email:ethan@umd.edu')
        
        # user's email should not be the bio
        self.assertNotEqual(self.ethan.email, 'I am Ethan')
        
    def test_find_dealbreakers(self):
        '''Tests that dealbreakers are found correctly'''
        dealbreaker1 = self.ethan.find_dealbreakers(self.brick.answers)
        dealbreaker2 = self.ethan.find_dealbreakers(self.numerah.answers)
        dealbreaker3 = self.brick.find_dealbreakers(self.ethan.answers)
        dealbreaker4 = self.ethan.find_dealbreakers(self.amber.answers)
        dealbreaker5 = self.sharon.find_dealbreakers(self.amber.answers)
        dealbreaker6 = self.josh.find_dealbreakers(self.amber.answers)
        dealbreaker7 = self.amber.find_dealbreakers(self.josh.answers)
        dealbreaker8 = self.josh.find_dealbreakers(self.numerah.answers)
        dealbreaker9 = self.sharon.find_dealbreakers(self.josh.answers)
        
        # Brick should contain one or more of Ethan's dealbreakers
        # because Brick is male and Ethan is interested in females
        self.assertFalse(dealbreaker1)
        
        # Numerah should not contain any of Ethan's dealbreakers
        self.assertTrue(dealbreaker2)
        
        # Ethan should not contain any of Bricks dealbreakers
        # because Ethan is male and Brick is interested in Males
        self.assertTrue(dealbreaker3)
        
        # Amber should not contain any of Ethan's dealbreakers
        self.assertTrue(dealbreaker4)
        
        # Amber should contain one or more of Sharon's dealbreakers
        # because Sharon is female and Amber is interested in males
        self.assertFalse(dealbreaker5)
        
        # Amber should contain one or more of Josh's dealbreakers
        # because Josh doesn't want someone who drinks and Amber does 
        self.assertFalse(dealbreaker6)
        
        # Josh should contain one or more of Amber's dealbreakers
        # because Josh smokes and Amber doesn't want someone who smokes
        self.assertFalse(dealbreaker7)
        
        # Numerah should contain one or more of Josh's dealbreakers
        # because Numerah has a pet and Josh doesn't want someone with a pet
        self.assertFalse(dealbreaker8)
        
        # Josh should contain oen or more of Sharon's dealbreakers
        # because Josh lives off campus and Sharon doesn't want 
        # someone who lives off campus 
        self.assertFalse(dealbreaker9)
        
    def test_find_matches(self):
        match1 = self.ethan.find_matches(self.numerah)
        match2 = self.ethan.find_matches(self.sharon)
        match3 = self.ethan.find_matches(self.amber)
        
        # Numerah's compatibility to Ethan should be an the number 94 as an int
        self.assertTrue(match1, int)
        self.assertEqual(94, match1)
        
        # compatibillity should not be a string
        self.assertNotEqual('94', match1)
        self.assertEqual(33, match2)
        self.assertEqual(28, match3)
        
        # compatibility should not be a decimal
        self.assertNotEqual(.94, match1)
        
    def test_matches(self):
        '''Tests that users are matches correctly'''
        name = self.ethan.name
        age = self.ethan.age
        answers = self.ethan.answers
        dealbreakers = self.ethan.dealbreakers
        email = self.ethan.email
        bio = self.ethan.bio
        
        potential_matches = [['Numerah', 22, ['b','b','c','a','a','a','a','a',
                                              'c','b','b','b','a','a','b','a',
                                              'b','b','a','b'], 
                              ['a','a','a','a'],'I am Numerah', 
                              'numerah@umd.edu'], 
                             ['Sharon', 20, ['b','b','b','b','b','b','b','b',
                                             'b','b','b','b','b','b','b','b',
                                             'b','b','b','b'],['a','a','a','a'],
                              'I am Sharon', 'sharon@umd.edu'],
                             ['Amber', 20, ['a','b','b','a','b','c','a','c','b',
                                          'a','b','a','a','c','a','c','b','a',
                                          'a','a'],['a','a','a','a'],
                              'I am Amber', 'amber@umd.edu']]
        final_matches = matches(name, age, answers, dealbreakers, email, bio,
                                potential_matches)
        
        # the final matches should be a list
        self.assertTrue(final_matches, list)
        
        # the list should contain lists
        self.assertTrue(final_matches[0], list)
        
        # Numerah should be Ethan's top match
        self.assertEqual(['Numerah', 'Female', 22, 94, 'I am Numerah',
                          'numerah@umd.edu'], final_matches[0])
        
        # Numerah and Sharon should be Ethan's only matches
        self.assertEqual([['Numerah', 'Female', 22, 94, 'I am Numerah', 
                           'numerah@umd.edu'],['Sharon', 'Female',20, 33, 
                                            'I am Sharon','sharon@umd.edu']], 
                         final_matches)
        
        # Numerah's name shouldn't be the only thing in the list
        self.assertNotEqual(['Numerah'], final_matches[0])
        
        # Amber should not be in Ethan's matches
        self.assertNotIn(['Amber', 20, 'Female', 28, 'I am Amber',
                          'amber@umd.edu'], final_matches)
    
class TestUmdateFiles(unittest.TestCase):
    '''Tests that the questions and answers text files are read and 
    written to correctly'''
    
    def setUp(self):
        '''
        Attributes:
            p_wrong (Path): path to text file with incorrect format
            p (Path): path to text file with correct formatting
        '''
        self.p_wrong = Path('wrong_questions_test.txt')
        with self.p_wrong.open('w', encoding= 'utf-8') as f:
            f.write('a,a,b,a,b; Dealbreakers:a,a,a,a; Email:ethan@umd.edu; ' 
                    'Bio:I like my dog more than I like women. I like to send '
                    'pictures of my food that doesn’t look that good. ')
        self.p = Path('questions_text.txt')
        with self.p.open('w+', encoding= 'utf-8') as f:
            f.write('Name:Kyle; Age:21; Answers:a,b,a,d,b,c,a,a,d,a,a,a,e,c,a,'
                    'c,a,a,b,a; Dealbreakers:b,a,a,a; Email:kyle@umd.edu; '
                    'Bio:I am in a frat\n')
            f.write('Name:Ethan; Age:20; Answers:a,a,c,a,a,a,a,a,c,b,b,b,a,a,'
                    'b,a,a,b,a,b; Dealbreakers:b,a,a,a; Email:ethan@umd.edu; '
                    'Bio:I am Ethan')
            
    def test_read_file(self):
        '''Tests that file is read and split up correctly'''
        correct_file = read_file(self.p)
        
        # Ethan's information should be the second thing in the list
        self.assertEqual(correct_file[1], ['Ethan', '20', ['a','a','c','a','a',
                                                           'a','a','a','c',
                                          'b','b','b','a','a','b','a','a','b',
                                          'a','b'], ['b','a','a','a'],
                            "I am Ethan", 'ethan@umd.edu'])
        
        # Kyle's information should be the first thing in the list, not the 2nd
        self.assertNotIn('Kyle', correct_file[1]) 
        self.assertNotEqual(correct_file[0], ['Kyle', '21', 
                                               ['a','b','a','d','b','c',
                                               'a','a','d','a','a','a','e','c',
                                               'a','c','a','a','b','a'],
                                               ['b','a','a','a'],
                                               'I am Kyle', 
                                               'kyle@umd.edu'])
        
        # The name:, age:, answers:, dealbreakers:, bio:, and email: 
        # portions should be removed 
        self.assertNotEqual(correct_file[0], ['Name:Kyle', 'Age:21', 
                                               ['Answers:a','b','a','d','b','c',
                                               'a','a','d','a','a','a','e','c',
                                               'a','c','a','a','b','a'],
                                               ['Dealbreakers:b','a','a','a'],
                                               'Bio:I am Kyle', 
                                               'Email:kyle@umd.edu'])
        
        # the wrong file should raise an IndexError when read in 
        with self.assertRaises(IndexError):
            read_file(self.p_wrong)
           
    def test_add_user(self):
        '''Tests that the file is written to correctly after user answers
        questions'''
        add_user('Amber', 20, ['a','b','b','a','b','c','a','c','b',
                                          'a','b','a','a','c','a','c','b','a',
                                          'a','a'],['a','a','a','a'],
                              'amber@umd.edu','I am Amber', str(self.p))
        with self.p.open(encoding = 'utf-8') as f:
            f = f.read()
            
        # file should now contain name:, age:, answers:, dealbreakers:, bio:, 
        # and email: 
        self.assertIn('Name:Amber; Age:20; Answers:a,b,b,a,b,c,a,c,b,a,b,a,a,c,'
                      'a,c,b,a,a,a; Dealbreakers:a,a,a,a; Email:amber@umd.edu; '
                      'Bio:I am Amber', f)
        self.assertNotIn('Amber;20;a,b,b,a,b,c,a,c,b,a,b,a,a,c,a,c,b,a,a,a;'
                            'a,a,a,a;amber@umd.edu;I am Amber', str(self.p))
        
    def tearDown(self):
        '''Unlinks files created in test script'''
        try:
            self.p_wrong.unlink()
            self.p.unlink()
        except:
            print('unable to delete temp files')
            pass  
      
if __name__ == '__main__':
 unittest.main()