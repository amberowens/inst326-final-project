import sys
""" A script to match users with other compatible people """

class Person:
    """ Represents a single person and their likes and dealbreakers
    
    Attributes:
        name (str): the name of the person
        age (int): the age of the person
        answers (list of strings): the person's answers to the questionnaire
        dealbreakers (list of strings): the person's dealbreakers
        email (str): a person's umd email
        bio (str): a brief bio of the user
    """
    
    def __init__(self, name, age, answers, dealbreakers, email, bio):
        self.name = name
        self.age = age
        self.answers = answers
        self.dealbreakers = dealbreakers
        self.gender = answers[0]
        self.email = email
        self.bio = bio
        
        if self.gender == 'a':
            self.gender = 'Male'
        elif self.gender == 'b':
            self.gender = 'Female'
    
    def find_dealbreakers(self, answer):
        """ Removes a potential match based on user dealbreakers
        
        Args:
            answer (list of strings): someone's answers
        
        Returns:
            True if a potential match doesn't have any of user's dealbreakers
            False if potential match does have one or more dealbreakers
        """
        dealbreaker_answer = answer[16:20]
        
        if self.answers[1] == 'a' and answer[0] == 'a':
            return False
        elif self.answers[1] == 'b' and answer[0] == 'b':
            return False
        elif self.dealbreakers[0] == "b" and dealbreaker_answer[0] == "a":
            return False
        elif self.dealbreakers[1] == "b" and dealbreaker_answer[1] == "a":
            return False
        elif self.dealbreakers[2] == "b" and dealbreaker_answer[2] == "a":
            return False
        elif self.dealbreakers[3] == "b" and dealbreaker_answer[3] == "b":
            return False
        else:
            return True
        
    
    def find_matches(self, potential_match):
        """ Determines percent compatibility for each potential match
        
        Args:
            potential_match (class): potential match as an instance in Person 
            class 
        
        Returns:
            (int): percent compatibility 
        """
        compatibility = 0
        index = 2
        
        for self_answer in self.answers[2:]:
            if self_answer == potential_match.answers[index]:
                compatibility += 1
            index += 1
            
        return round((compatibility/18) * 100)
    
def matches(user_name, user_age, user_answers, user_dealbreakers, user_email,
            user_bio, potential_matches):
    """ Sorts matches based on compatibility 
    
    Args:
        user_name (str): the name of the user
        user_age (int): the age of tge user 
        user_answers (list of strings): the user's answers to the questionnare
        user_dealbreakers (list of strings): the user's dealbreakers
        user_email (str): the user's umd email
        user_bio (str): a brief user bio
        potential_matches (list of lists): the potential matches
        
    Returns:
        list of matches sorted by compatibility 
    """
    matches = []
    user = Person(user_name, user_age, user_answers, user_dealbreakers, 
                  user_email, user_bio)
    for person in potential_matches:
        details = []
        person = Person(person[0], person[1], person[2], person[3], person[4],
                        person[5])
        if user.find_dealbreakers(person.answers) == True:
            if person.find_dealbreakers(user.answers) == True:
                compatibility = user.find_matches(person)
                details.append(person.name)
                details.append(person.gender)
                details.append(person.age)
                details.append(compatibility)
                details.append(person.email)
                details.append(person.bio)
                
                matches.append(details)
    
    matches = sorted(matches, key = lambda person: person[3], reverse = True)
                      
    return matches
            
               
def read_file(f):
    """ Reads in file with potential matches and their information
    
    Args:
        f (str): path to file
        
    Returns:
        list of lists containing each user and their information 
    """
    
    with open(f, encoding='utf-8') as f:
        users = []
        for line in f:
            info = []
            line = line.split(';')
            name = line[0][5:]
            age = line[1][5:]
            answers = line[2][9:].split(',')
            dealbreakers = (line[3][14:].strip('\n').split(','))
            if dealbreakers == {''}:
                dealbreakers = []
            email = line[4][7:].strip()
            bio = line[5][5:].strip()
            info.append(name)
            info.append(age)
            info.append(answers)
            info.append(dealbreakers)
            info.append(bio)
            info.append(email)
            users.append(info)
  
    return(users)


def questionniare(f):
    """ Asks user questions about themselves and stores answers
    
    Args:
        f (str): path to file containing questions
    
    Returns:
        list of user's answers to the questionnaire
    """
    with open(f, encoding='utf-8') as f:
        answers = []
        
        for line in f:
            options = []
            line = line.strip('\n').split(',')
            question = f'{line[0]}'
            
            while True:
                print(question)
            
                for q in line[1:]:
                    option = q[1]
                    options.append(option)
                    print(q)

                print('')
                answer = input('Answer: ')
                answer = answer.lower()
                print('––––––––––––––––––––––––––––––')
                
                if answer not in options:
                    print('** Invalid input, try again **')
                    continue
                answers.append(answer)
                break
            
    return answers


def display_matches(lst, user_name):
    """ Displays the most compatible users, their info, and their percent
        compatibility to the user
    Args:
        lst (list of lists): list of matches returned by matches()
        name (str): name of the user

    Side effects:
        Prints their most compatible match's name, gender, age, and percent
        compatibility
    """

    if lst == []:
        print('')
        print('Calculating match...')
        print('')
        print(f"Congrats {user_name}! You'll be alone forever!")
        print('You have no matches at this time haha')
        print('––––––––––––––––––––––––––––––')
            
    else:
        match = lst[0]
        name = match[0]
        gender = match[1]
        age = match[2]
        compatibility = match[3]
        bio = match[4]
        email = match[5]
        
        index = 1
        
        
        print('')
        print('Calculating matches...')
        print('')
        print(f'Congrats {user_name}! Your top match with {compatibility}% '
            f'compatibility is...')
        print('')
        print(f'Name: {name}')
        print(f'Gender: {gender}')
        print(f'Age: {age}')
        print(f'Email: {email}')
        print(f'Bio: {bio}')
        
        print('––––––––––––––––––––––––––––––')
        
        
        while True:
            try: 
                print('')
                print('Would you like to see another match?')
                print('a.) yes')
                print('b.) no')
                print('')
                answer = input('Answer: ')
                print('')
                print('––––––––––––––––––––––––––––––')
                if answer == 'a':
                    match = lst[index]
                    name = match[0]
                    gender = match[1]
                    age = match[2]
                    compatibility = match[3]
                    bio = match[4]
                    email = match[5]
                    print('')
                    print(f'{compatibility}% compatability...')
                    print('')
                    print(f'Name: {name}')
                    print(f'Gender: {gender}')
                    print(f'Age: {age}')
                    print(f'Email: {email}')
                    print(f'Bio: {bio}')
                    print('––––––––––––––––––––––––––––––')
                    index += 1
                    continue 
                elif answer == 'b':
                    print('Thanks for using UMDate! Feel free to ' )
                    print('contact your matches through UMD email.')
                    print('')
                    break
                else:
                    print('––––––––––––––––––––––––––––––')
                    print("Invalid input. Please type 'a' or 'b'")
                    continue
                
            except IndexError:
                print('End of matches')
                print('')
                print('Thanks for using UMDate! Feel free to ' )
                print('contact your matches through UMD email.')
                print('')
                break
                   
def add_user(name, age, answers, dealbreakers, email, bio, answers_file):
    """ Writes the user's information to the file
  
    Args:
        name (str): the user's name
        age (float): the user's age
        answers (list): the user's answers to the questionnaire
        dealbreakers (list): the user's dealbreakers
        email (str): user's umd email
        bio (str): the user's brief bio
        answers_file (str): path to the file of all user answers
      
    Side effects:
        writes user's information to the file
      
    """
    with open(answers_file, 'a', encoding = 'utf-8') as f:
        answers = (',').join(answers)
        dealbreakers = (',').join(dealbreakers)
        f.write(f'\nName:{name}; Age:{age}; Answers:{answers}; '
               f'Dealbreakers:{dealbreakers}; Email:{email}; Bio:{bio}')


def main(questions_file, answers_file):
    """ Connects all classes, methods, and functions
    
    Args:
        questions_file (str): path to questions file
        answers_file (str): path to answers file
        
    Side effects:
        prints program to terminal 
    """
    
    print('––––––––––––––––––––––––––––––')
    print('\033[0;30;47mWelcome to UMDate! \n')
    print('')
    print('Instructions:')
    print('')
    print(('You will be asked a series of questions about yourself and your '),
          ('dating preferences.'))
    print(("Please only type the letter corresponding to your answer and hit "),
        ("'enter'."))
    print('')
        
    while True:  
        enter = input(("Press 'enter' to begin the questionnaire > "))
        if enter != '':
            continue
        break
    
    while True:
        print('––––––––––––––––––––––––––––––')
        email = input('Please enter your UMD email: ') 
        if '@umd.edu' in email or '@terpmail.umd.edu' in email:
            break
        else:
            print('Email is not a UMD email. Please try again')
    
    while True:
        print('––––––––––––––––––––––––––––––')
        name = input('What is your name? ')
        if name.lower().replace(' ','').isalpha() != True:
            print('––––––––––––––––––––––––––––––')
            print('** Please only enter letters')
            print('for your name. Try again **')
            continue
        break
    
    while True:  
        print('––––––––––––––––––––––––––––––')
        age = input('How old are you? ')
        try:
            age = int(age)
            if age < 18:
                print('––––––––––––––––––––––––––––––')
                print('')
                print('** Sorry, you must be over 18 to use UMDate **')
                print('')
                sys.exit()
            elif age > 25:
                print('––––––––––––––––––––––––––––––')
                print('')
                print("** Sorry, you must be 25 or under to use UMDate **")
                print('')
                sys.exit()
            break
        except ValueError:
            print('––––––––––––––––––––––––––––––')
            print('** Please enter a correct number. ')
            print('Try again **')
            continue
        
            
        
    print('––––––––––––––––––––––––––––––')
    
    bio = input('Write a little about yourself: ')
    
    print('––––––––––––––––––––––––––––––')
    
    answers = questionniare(questions_file)
   
    dealbreakers = answers[20:]
    answers = answers[:20]
    
    potential_matches = read_file(answers_file)
    
    match = matches(name, age, answers, dealbreakers, email, bio, potential_matches)
    
    add_user(name, age, answers, dealbreakers, email, bio, answers_file)
   
    display_matches(match, name)
    

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
    
    

